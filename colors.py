from dataclasses import dataclass
from css3_extended_keyword_colors import data as css3_color_data


@dataclass
class Color:
    name: str
    hex: str
    rgb: tuple

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'<Color {self.name} hex="{self.hex}" rgb={self.rgb}>'

    def __hash__(self):
        return hash(repr(self))


class Palette:
    colors = {}

    def __init__(self, color_data):
        for color in color_data:
            setattr(self, color.name, color)
        # prefer alphabetical key insertion order
        self.colors = dict(sorted({
            attr_key: attr_value for attr_key, attr_value in self.__dict__.items() if type(attr_value) == Color
        }.items()))

    def __len__(self):
        return len(self.colors)

    def __repr__(self):
        type_ = type(self)
        module = type_.__module__
        qualname = type_.__qualname__
        return f"<{module}.{qualname} object at {hex(id(self))} with {len(self)} Color(s)>"


css = Palette(
    Color(name=color_data[0], hex=color_data[1], rgb=color_data[2]) for color_data in css3_color_data
)
