from random import choice, choices, sample
from colors import css


def main():
    """function to test the basic capability of A Named Colors Palette"""
    print(css)
    print(repr(css.red))
    print(css.red)
    print(css.red.name)
    print(css.red.hex)
    print(css.red.rgb)

    # pick a random color
    print(choice(sorted(css.colors)))
    # pick two unique colors
    foreground, background = sample(sorted(css.colors), 2)
    print(foreground, background)


if __name__ == '__main__':
    main()
