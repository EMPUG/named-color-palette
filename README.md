# Named Colors Palette

For the purpose of having a data structure where color names of a given palette are attributes. Theoretically that might allow for IDE autocomplete. Although in PyCharm this is not the case currently.

This might not be a great idea in general, but is sort of interesting and was a fun exercise where we create a dataclass and also a few other _tricks_.

I'm not sure how something like this _should_ be distributed so I'll explain the current file structure.

1. There is a `main.py` module that imports the `css` Palette object and illustrates the essential usage.
2. There is a `css3_extended_keyword_colors.py` module that is a tuple of tuples (which also contain one tuple on ints) that is data copy and pasted from the CSS3 spec page. I think it was this one but I might be wrong. [https://w3c.github.io/csswg-drafts/css-color/#named-colors](https://w3c.github.io/csswg-drafts/css-color/#named-colors)
3. There is a `colors.py` module where the _action_ is. It defines the Color dataclass and the Palette class that colors colors as attributes. I added an alphabetically sorted dictionary of the names:color_obj on the palette, `palette.colors` among the many conveniences this provides is easy collection fun like `random.choice`. In the process I also made Color hashable as I was momentarily thinking about making it a set instead of a dict, since the color object is not the key in the dict, it does not need to be hashable but it seems worth keeping for reference.

Of course the CSS3 Extended Keyword Color Palette is not necessarily ideal for anything in particular. However, it is the first usefully large enough set of named colors that comes to mind.

I am interested in adding additional pallets, and I am also interested in general feedback and suggestions. Feel free to open an issue.


I also have a few things to follow up on:

* Consider the color set this developer used, at a glance it looks similar but larger. Also their constants -> dict methodology is interesting, but I don't think I am likely to abandon the class concept because I have ideas on ways to expand the functionality with generating sub sets of colors based on different criteria. [https://www.webucator.com/article/python-color-constants-module/](https://www.webucator.com/article/python-color-constants-module/)

* Consider the colour lib, it has a very different purpose but it might also be interesting to work with the library and my modules together. [https://www.colour-science.org/colour-datasets/](https://www.colour-science.org/colour-datasets/)
